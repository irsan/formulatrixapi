﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FormulatrixAPI.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace FormulatrixAPI.Tests.Controllers
{
    [TestClass]
    public class FormulatrixControllerTest
    {
        [TestMethod]
        public void RegisterJson()
        {
            var controller = new FormulatrixController();

            dynamic testObject = new
            {
                name = "testjson1",
                value = "testvalue1"
            };
            var itemName = "TestJson1";
            var itemContent = JsonConvert.SerializeObject(testObject);
            controller.Register(itemName, itemContent, 1);

            var testObjectValue = controller.Retrieve(itemName);
            Assert.AreEqual(controller.GetType(itemName), 1);
            Assert.AreEqual($@"{itemName} {itemContent} 1", testObjectValue);

            controller.Deregister(itemName);
        }

        [TestMethod]
        public void RegisterXml()
        {
            var controller = new FormulatrixController();

            var itemName = "TestXml1";
            var itemContent = "<ROOT><Employee Id=\"100\"/><Employee Id=\"134\"/><Employee Id=\"178\"/></ROOT>";
            
            controller.Register(itemName, itemContent, 2);

            var testObjectValue = controller.Retrieve(itemName);
            Assert.AreEqual(controller.GetType(itemName), 2);
            Assert.AreEqual($@"{itemName} {itemContent} 2", testObjectValue);

            controller.Deregister(itemName);
        }
    }
}
