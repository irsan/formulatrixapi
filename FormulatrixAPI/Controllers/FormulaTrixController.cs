﻿using System;
using System.Web.Http;
using System.Data.SQLite;
using System.Xml;
using FormulatrixAPI.Helpers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace FormulatrixAPI.Controllers
{
    [RoutePrefix("api/formulatrix")]
    public class FormulatrixController : ApiController, IDisposable
    {
        private static SQLiteConnection connection;

        public FormulatrixController()
        {
            Initialize();
        }

        public void Register(string itemName, string itemContent, int itemType)
        {
            switch (itemType)
            {
                case 1: 
                    Console.WriteLine("this is JSON");
                    if (IsValidJson(itemContent))
                    {
                        InsertIntoDb(itemName, itemContent, itemType);
                    }
                    break;
                case 2:
                    Console.WriteLine("this is XML");
                    if (IsValidXml(itemContent))
                    {
                        InsertIntoDb(itemName, itemContent, itemType);
                    }
                    break;
            }
        }

        public string Retrieve(string itemName)
        {
            var result = string.Empty;

            connection.Open();

            var command = new SQLiteCommand(connection);

            command.CommandText = $@"SELECT * FROM Item WHERE itemName = '{itemName}'";
            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    result += $@"{reader.GetString(0)} {reader.GetString(1)} {reader.GetInt32(2)}";
                }
            }

            connection.Close();

            return result;
        }

        public int GetType(string itemName)
        {
            var result = 0;

            connection.Open();

            var command = new SQLiteCommand(connection);

            command.CommandText = $@"SELECT itemType FROM Item WHERE itemName = '{itemName}'";
            result = Convert.ToInt32(command.ExecuteScalar());

            connection.Close();

            return result;
        }

        public void Deregister(string itemName)
        {
            connection.Open();

            var command = new SQLiteCommand(connection);

            command.CommandText = $@"DELETE FROM Item WHERE itemName = '{itemName}'";
            command.ExecuteNonQuery();

            Console.WriteLine($@"Item with item name {itemName} has been deregister");

            connection.Close();
        }

        public void Initialize()
        {
            if (connection == null)
            {
                connection = ConnectionHelpers.GetConnection();
                CreateMockUpDb();
            }
        }

        private void CreateMockUpDb()
        {
            connection.Open();
            var cmd = new SQLiteCommand(connection);

            cmd.CommandText = @"DROP TABLE IF EXISTS Item";
            cmd.ExecuteNonQuery();

            cmd.CommandText = @"CREATE TABLE Item(itemName TEXT NOT NULL PRIMARY KEY, itemContent TEXT, itemType INT)";
            cmd.ExecuteNonQuery();
            connection.Close();
        }

        private bool IsValidXml(string itemContent)
        {
            try
            {
                if (!string.IsNullOrEmpty(itemContent))
                {
                    System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
                    xmlDoc.LoadXml(itemContent);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (System.Xml.XmlException ex)
            {
                return false;
            }
        }

        private bool IsValidJson(string itemContent)
        {
            if (string.IsNullOrWhiteSpace(itemContent)) { return false; }
            itemContent = itemContent.Trim();
            if ((itemContent.StartsWith("{") && itemContent.EndsWith("}")) || //For object
                (itemContent.StartsWith("[") && itemContent.EndsWith("]"))) //For array
            {
                try
                {
                    var obj = JToken.Parse(itemContent);
                    return true;
                }
                catch (JsonReaderException jex)
                {
                    //Exception in parsing json
                    Console.WriteLine(jex.Message);
                    return false;
                }
                catch (Exception ex) //some other exception
                {
                    Console.WriteLine(ex.ToString());
                    return false;
                }
            }
            return false;
        }

        private void InsertIntoDb(string itemName, string itemContent, int itemType)
        {
            connection.Open();
            var command = new SQLiteCommand(connection);

            command.CommandText = $"SELECT * FROM Item WHERE itemName = '{itemName}'";
            using (var reader = command.ExecuteReader())
            {
                var isAlreadyExists = reader.HasRows;

                if (isAlreadyExists)
                {
                    Console.WriteLine("Unable to update or insert registered items");
                    return;
                }
            }

            command.CommandText = $@"INSERT INTO Item (itemName, itemContent, itemType) VALUES('{itemName}', '{itemContent}', {itemType})";
            command.ExecuteNonQuery();
            connection.Close();
        }
    }
}
