﻿using System.Data.SQLite;

namespace FormulatrixAPI.Helpers
{
    public static class ConnectionHelpers
    {
        private const string CONN_STRING = @"Data Source=testdb.sqlite";
        private static SQLiteConnection _connection;
        public static SQLiteConnection GetConnection()
        {
            if (_connection == null)
            {
                SQLiteConnection.CreateFile("testdb.sqlite");
                _connection = new SQLiteConnection(CONN_STRING);
            }

            return _connection;
        }
    }
}